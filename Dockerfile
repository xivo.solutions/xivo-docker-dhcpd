FROM python:3.9-bullseye

ADD http://mirror.xivo.solutions/xivo_current.key /tmp
ADD xivo-dist.list /etc/apt/sources.list.d/
RUN apt-key add /tmp/xivo_current.key

RUN   apt-get -yq update \
   && apt-get -yqq install \
      libldap2-dev \
      libsasl2-dev \
      git \
      gcc \
      isc-dhcp-server \
      iproute2 \
      xivo-dhcpd-update

WORKDIR /usr/local/src
COPY requirements.txt .
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --no-cache-dir -r requirements.txt && \
	ln -s /usr/local/bin/xivo-dxtorc /usr/bin

RUN rm -rf -- /etc/dhcp/*      
ADD docker-entrypoint.sh /
ADD etc /etc




# install xivo-container-helper
WORKDIR /usr/share
ADD https://gitlab.com/xivo.solutions/xivo-container-helper/-/archive/master/xivo-container-helper-master.tar.gz .
RUN     tar -xzf xivo-container-helper-master.tar.gz
RUN     rm xivo-container-helper-master.tar.gz 
RUN     ln -s /usr/share/xivo-container-helper-master/xivo-entrypoint.sh / 
RUN     mkdir /xivo-entrypoint.d

# init entrypoint scripts
RUN ln -s /usr/share/xivo-container-helper-master/xivo-entrypoint.d/templates.sh /xivo-entrypoint.d/10-templates.sh && \
    ln -s /docker-entrypoint.sh /xivo-entrypoint.d/90-entrypoint-orig.sh


ENTRYPOINT ["/xivo-entrypoint.sh"]
